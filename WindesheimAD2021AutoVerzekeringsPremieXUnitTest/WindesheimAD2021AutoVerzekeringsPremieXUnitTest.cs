using System;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;

namespace WindesheimAD2021AutoVerzekeringsPremieXUnitTest
{
    public class PremiumCalculationTest
    {
        //this test is for testing if the PremiumBaseCalculation is done correctly
        [Fact]
        public void BasePremiumCalculatedCorrectly()
        {
            // Arrange
            Vehicle car = new Vehicle(PowerInKw: 60, ValueInEuros: 2000, constructionYear: DateTime.Now.Year - 5);

            // Act
            double result = PremiumCalculation.CalculateBasePremium(car);

            // Assert
            Assert.Equal(9, result);
        }


        //this test is to see if the discount is being updated when personhas more than 5 no claim years
        [Theory]
        [InlineData(-10, 20.29)]
        [InlineData(0, 20.29)]
        [InlineData(10, 15.21)]
        [InlineData(65, 7.1)]
        public void DiscountUpdateWhenPersonHasMoreThanFiveNoClaimYears(int years, double ExpectedOutcome)
        {
            //Vehicle
            Vehicle testVehicle = new Vehicle(150, 3000, 2005);

            //Policyholder
            PolicyHolder testPolicyHolder = new PolicyHolder(23, "19-01-2016", 1325, years);

            //PremiumCalculation
            PremiumCalculation Outcome = new(testVehicle, testPolicyHolder, InsuranceCoverage.WA_PLUS);

            //Calculate and round to 2 decimals
            Assert.Equal(ExpectedOutcome, Math.Round(Outcome.PremiumAmountPerYear, 2));
        }

    }

    public class PolicyHolderTest
    {
        //this test is for testing if the user-given drivers-license startdate is correct
        [Theory]
        [InlineData(23, "19-01-2016", 1325, 3, 5)]
        [InlineData(40, "01-10-2000", 1930, 100, 20)]
        [InlineData(30, "10-07-2022", 1940, 20, -2)]
        public void PolicyHolderLicenseAgeIsCorrect(int Age, string DriverlicenseStartDate, int PostalCode, int NoClaimYears, int expectedValue)
        {
            // Arrange

            var policyholder = new PolicyHolder(Age, DriverlicenseStartDate, PostalCode, NoClaimYears);

            // Act
            var InputValue = policyholder.LicenseAge;

            //Assert
            Assert.Equal(expectedValue, InputValue);

        }
    }

    public class VehicleTest
    {
        //this test is for testing if the user-given vehicle construction year is correct
        [Theory]
        [InlineData(60, 3000, 0, 2021)]
        [InlineData(190, 5000, 2013, 8)]
        [InlineData(410, 45000, 2030, 0)]
        public void VehicleAgeIsCorrect(int PowerInKw, int ValueInEuros, int constructionYear, int expectedValue)
        {
            // Arrange

            var vehicle = new Vehicle(PowerInKw, ValueInEuros, constructionYear);

            // Act
            var InputValue = vehicle.Age;

            //Assert
            Assert.Equal(expectedValue, InputValue);
        }

    }
}

