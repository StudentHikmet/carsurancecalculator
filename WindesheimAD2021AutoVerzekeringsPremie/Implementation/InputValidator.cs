﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    class InputValidator
    {
        public bool AgeValidator(string input, out int age)
        {
            if (int.TryParse(input, out int UnvalidatedAge) && UnvalidatedAge >= 0)
            {
                age = UnvalidatedAge;
                return true;
            }
            age = 0;
            return false;
        }
    }
}
